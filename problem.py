import ast

def parents(array, child):
    #Enumerates on the list of all the parents of a child
    parent = array[child]
    while child != parent:
        yield parent
        child = parent
        parent = array[child]


def update_child_count(parent_node, count_node, child):
    for parent in parents(parent_node, child):
        count_node[parent] += 1 


if __name__ == '__main__':
    # with open('input.txt','r') as myfile:
    #     length = sum(1 for line in myfile)
    with open('input.txt','r') as input_file:
        #lst = [[] for _ in range(length)]
        lst = []
        for line in input_file:
            flattened_list = []
            for j, value in enumerate(line.split()):
                flattened_list.append(int(value))
            lst.append(flattened_list)
        #print(lst)
                
        N, E = map(int, lst[0])
        #print(N,E)
        count_node = [None]*N
        parent_node = [None]*N
        value_root = 0
        result = 0
        for child, parent in enumerate(parent_node):
            parent_node[child] = child
            count_node[child] = 1
        i=1
        for node in range(E):
            child, parent = map(int, lst[i])
            i=i+1
            parent_node[child-1] = parent-1
            update_child_count(parent_node, count_node, child-1)
        #count removed edges here
        for child, parent in enumerate(count_node):
            if count_node[child] % 2 == 0 and child != value_root:
                result += 1
        #print(result)
        f = open("output.txt", "w")
        f.write(str(result))      # str() converts to string
        f.close()